# Marlin for CR-10 + BLTOUCH

## Getting started

1. Download [Arduino](https://www.arduino.cc/en/Main/Software)
1. Add [Sanguino](https://github.com/Lauszus/sanguino) to the board manager in Arduino
1. Download [Marlin](http://marlinfw.org/meta/download/)
1. Drop both `Configuration.h` and `Configuration_adv.h` into the Marlin directory
1. Verify and Compile
